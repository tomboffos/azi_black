<?php

namespace App\Models;

use App\Contracts\FriendshipContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Friendship extends Model
{
    use HasFactory,SoftDeletes;

    protected $fillable = FriendshipContract::FILLABLE;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function friend()
    {
        return $this->belongsTo(User::class,FriendshipContract::FRIEND_ID);
    }
}
