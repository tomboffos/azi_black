<?php

namespace App\Repositories\Interfaces;

use App\Http\Requests\Api\Payment\WithdrawStoreRequest;
use App\Models\User;
use App\Models\Withdraw;

interface WithdrawRepositoryInterface
{

    public function withdraw(WithdrawStoreRequest $request);

    public function checkUserBalance(User $user,Withdraw $withdraw);


    public function calculatePercentage($amount);

}
