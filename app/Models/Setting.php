<?php

namespace App\Models;

use App\Contracts\SettingContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Setting extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * @return array
     */
    protected $fillable = SettingContract::FILLABLE;
}
