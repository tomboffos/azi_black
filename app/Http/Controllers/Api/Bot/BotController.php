<?php

namespace App\Http\Controllers\Api\Bot;

use App\Http\Controllers\Controller;
use App\Models\Game;
use App\Repositories\Interfaces\BotRepositoryInterface;
use Illuminate\Http\Request;

class BotController extends Controller
{
    private $botRepository;

    public function __construct(BotRepositoryInterface $botRepository)
    {
        $this->botRepository = $botRepository;
    }

    //
    public function join(Game $game)
    {
       return $this->botRepository->attachBotToRoom($game);
    }
}
