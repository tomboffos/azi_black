<?php

namespace App\Console\Commands;

use App\Models\GameMember;
use Carbon\Carbon;
use Illuminate\Console\Command;

class UsersEntryCheck extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'entry:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        foreach (GameMember::where('ready', 0)->where('created_at', '<', Carbon::now()->subHours(1))->get() as $member) {
            $member->delete();
        }


        return 0;
    }
}
