<?php

namespace App\Observers;

use App\Http\Requests\Game\StoreGameRequest;
use App\Http\Resources\Game\GameResource;
use App\Models\Game;
use App\Models\User;
use App\Repositories\Interfaces\BotRepositoryInterface;
use App\Repositories\Interfaces\GameRepositoryInterface;
use App\Service\GameService;
use App\Service\WebSocketService;

class GameObserver
{
    private $gameRepository;

    public function __construct(GameRepositoryInterface $gameRepository)
    {
        $this->gameRepository = $gameRepository;
    }

    /**
     * Handle the Game "created" event.
     *
     * @param \App\Models\Game $game
     * @return void
     */
    public function created(Game $game)
    {
        //

    }

    /**
     * Handle the Game "updated" event.
     *
     * @param \App\Models\Game $game
     * @return void
     */
    public function updated(Game $game)
    {
        if ($game->getOriginal('started_at') != $game->started_at) {
            $gameService = new GameService();

            $gameService->gameStarted($game);
        }

        if ($game->getOriginal('finished_at') != $game->finished_at) {
            WebSocketService::sendEvent('azi.game.finished.' . $game->id, new GameResource($game));
            if (!is_null($game->getBot())) {
                $this->gameRepository->leaveBot($game->room, User::find($game->getBot()->user_id));
            }

            sleep(10);


            WebSocketService::sendEvent('azi.game.recreated.' . $game->id, new GameResource($game));

        }


        // TODO Add notification game started niggas
    }

    /**
     * Handle the Game "deleted" event.
     *
     * @param \App\Models\Game $game
     * @return void
     */
    public function deleted(Game $game)
    {
        //
    }

    /**
     * Handle the Game "restored" event.
     *
     * @param \App\Models\Game $game
     * @return void
     */
    public function restored(Game $game)
    {
        //
    }

    /**
     * Handle the Game "force deleted" event.
     *
     * @param \App\Models\Game $game
     * @return void
     */
    public function forceDeleted(Game $game)
    {
        //
    }
}
