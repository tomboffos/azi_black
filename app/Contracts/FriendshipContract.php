<?php


namespace App\Contracts;


interface FriendshipContract
{
    const USER_ID = 'user_id';
    const FRIEND_ID = 'friend_id';

    const CONFIRMED = 'confirmed';
    const DELETED_AT = 'deleted_at';
    const FILLABLE = [
        self::USER_ID,
        self::FRIEND_ID,
        self::CONFIRMED,
        self::DELETED_AT
    ];
}
