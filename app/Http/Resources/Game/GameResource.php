<?php

namespace App\Http\Resources\Game;

use App\Contracts\GameContract;
use App\Contracts\GameMemberContract;
use App\Http\Resources\GameMemberResource;
use App\Http\Resources\Room\RoomResource;
use App\Models\Room;
use Illuminate\Http\Resources\Json\JsonResource;

class GameResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $members = $this->gameMembers();
        return [
            'id' => $this->id,
            GameContract::BID => $this->bid,
            GameContract::CENTRAL_CARD => $this->central_card,
            GameContract::STARTED_AT => $this->started_at,
            GameContract::FINISHED_AT => $this->finished_at,
            GameContract::ROOM_ID => new RoomResource(Room::find($this->room_id)),
            'members' => GameMemberResource::collection($members->get()),
            'user' => new GameMemberResource($this->gameMembers()->where('user_id',$request->user()->id)->first())
        ];
    }
}

