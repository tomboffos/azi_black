<?php


namespace App\Contracts;


interface UserContract
{
    const NAME = 'name';
    const EMAIL = 'email';
    const PASSWORD = 'password';
    const BALANCE = 'balance';
    const REMEMBER_TOKEN = 'remember_token';
    const EMAIL_VERIFIED_AT = 'email_verified_at';
    const FAKE_BALANCE = 'fake_balance';
    const DEVICE = 'device';

    const FILLABLE = [
        self::NAME,
        self::EMAIL,
        self::PASSWORD,
        self::BALANCE,
        self::FAKE_BALANCE,
        self::DEVICE
    ];

    const HIDDEN = [
        self::PASSWORD,
        self::REMEMBER_TOKEN
    ];

    const CASTS = [
        self::EMAIL_VERIFIED_AT => 'datetime'
    ];
}
