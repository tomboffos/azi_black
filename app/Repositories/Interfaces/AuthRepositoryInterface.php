<?php


namespace App\Repositories\Interfaces;


use App\Http\Requests\Api\Auth\LoginRequest;
use App\Http\Requests\Api\Auth\RegisterRequest;
use Illuminate\Http\Request;

interface AuthRepositoryInterface
{
    public function firstRegister(Request $request);

    public function registration(RegisterRequest $request);

    public function login(LoginRequest $request);
}
