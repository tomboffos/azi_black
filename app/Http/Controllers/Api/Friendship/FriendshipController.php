<?php

namespace App\Http\Controllers\Api\Friendship;

use App\Http\Controllers\Controller;
use App\Http\Requests\Friendship\InviteRequest;
use App\Http\Requests\FriendshipIndexRequest;
use App\Http\Requests\Friendships\StoreFriendshipRequest;
use App\Models\Friendship;
use App\Repositories\Interfaces\FriendshipRepositoryInterface;
use Illuminate\Http\Request;

class FriendshipController extends Controller
{
    protected $friendshipRepository;

    public function __construct(FriendshipRepositoryInterface $friendshipRepository)
    {
        $this->friendshipRepository = $friendshipRepository;
    }

    public function search(Request $request)
    {
        return $this->friendshipRepository->search($request);
    }


    public function index(FriendshipIndexRequest $request)
    {

        return  $this->friendshipRepository->friendsList($request);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreFriendshipRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreFriendshipRequest $request)
    {
        return $this->friendshipRepository->addFriend($request);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Friendship $friendship
     * @return \Illuminate\Http\Response
     */
    public function show(Friendship $friendship)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Friendship $friendship
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Friendship $friendship)
    {
        //
    }


    public function destroy(Friendship $friend)
    {
        return $this->friendshipRepository->deleteFriend($friend);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Friendship $friendship
     * @return \Illuminate\Http\Response
     */
    public function confirm(Friendship $friendship)
    {
        return $this->friendshipRepository->confirmRequest($friendship);
    }


    public function invite(InviteRequest $request)
    {
        $this->friendshipRepository->inviteFriend($request);

        return response(['message' => 'Sent'], 200);
    }

    public function offers(Request $request)
    {
        return $this->friendshipRepository->offers($request);
    }

    public function requests(Request $request)
    {
        return $this->friendshipRepository->requests($request);
    }
}
