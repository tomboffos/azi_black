<?php


namespace App\Service;


use App\Contracts\GameMemberContract;
use App\Models\GameMember;

class CardsService
{
    public $cards = [
        [
            'suit' => 'spade',
            'seniority' => 'ace'
        ],
        [
            'suit' => 'spade',
            'seniority' => 'king'
        ],
        [
            'suit' => 'spade',
            'seniority' => 'queen'
        ],
        [
            'suit' => 'spade',
            'seniority' => 'jack'
        ],
        [
            'suit' => 'spade',
            'seniority' => '10'
        ],


        [
            'suit' => 'hearts',
            'seniority' => 'ace'
        ],
        [
            'suit' => 'hearts',
            'seniority' => 'king'
        ],
        [
            'suit' => 'hearts',
            'seniority' => 'queen'
        ],
        [
            'suit' => 'hearts',
            'seniority' => 'jack'
        ],
        [
            'suit' => 'hearts',
            'seniority' => '10'
        ],

        [
            'suit' => 'clubs',
            'seniority' => 'ace'
        ],
        [
            'suit' => 'clubs',
            'seniority' => 'king'
        ],
        [
            'suit' => 'clubs',
            'seniority' => 'queen'
        ],
        [
            'suit' => 'clubs',
            'seniority' => 'jack'
        ],
        [
            'suit' => 'clubs',
            'seniority' => '10'
        ],

        [
            'suit' => 'diamonds',
            'seniority' => 'ace'
        ],
        [
            'suit' => 'diamonds',
            'seniority' => 'king'
        ],
        [
            'suit' => 'diamonds',
            'seniority' => 'queen'
        ],
        [
            'suit' => 'diamonds',
            'seniority' => 'jack'
        ],
        [
            'suit' => 'diamonds',
            'seniority' => '10'
        ],

    ];

    public $currentCards = [];

    public function __construct()
    {
        $this->currentCards = $this->cards;
    }

    private $centralCard;

    public function getRandomCardFromDeck($getCentralCard = false)
    {
        $rand = mt_rand(0, count($this->currentCards) - 1);
        $centralCard = $this->currentCards[$rand];
        if ($getCentralCard) {

            $this->centralCard = $centralCard;
        }

        unset($this->currentCards[$rand]);

        $this->currentCards = array_values($this->currentCards);


        return $centralCard;
    }

    public function givePointToUser(GameMember $gameMember, int $maxPoints): void
    {
        if ($maxPoints == 33)
            return;

        $sum = 0;


        $cards = [];

        $rand = mt_rand(1, 100);

        if ($rand <= 55) {
            while ($sum <= $maxPoints) {
                $cards = $this->giveCardsForUser();

                $sum = $this->calculateCardsPoint($cards);

                if ($sum <= $maxPoints)
                    foreach ($cards as $card) {
                        $this->currentCards[] = $card;
                    }
            }

            $gameMember->update([
                GameMemberContract::CARDS => $cards,
                GameMemberContract::POINTS => $sum,
            ]);
        }



    }


    public function giveCardsForUser()
    {
        $cards = [];
        for ($i = 0; $i < 3; $i++) {
            $cards[] = $this->getRandomCardFromDeck();
        }

        return $cards;
    }

    public function calculateCardsPoint($cards)
    {
        $pointsOfCards = [];

        foreach ($cards as $card) {
            $suits = $this->getSameCards('suit', $cards, $card);

            $seniorities = $this->getSameCards('seniority', $cards, $card);

            $acesCountInSuits = $this->calculateAcesCount($suits);

            $acesCountInSeniorities = $this->calculateAcesCount($seniorities);

            $suitPoints = count($suits) * 10 + count($acesCountInSuits) + $this->getSameCard($card);

            $seniorityPoints = count($seniorities) * 10 + count($acesCountInSeniorities) + $this->getSameCard($card);

            $pointsOfCards[] = $suitPoints > $seniorityPoints ? $suitPoints : $seniorityPoints;
        }

        return max($pointsOfCards);

    }

    public function getSameCard($card)
    {
        if ($card['seniority'] == '10' && $this->centralCard['suit'] == $card['suit'])
            return 1;
        return 0;
    }

    public function calculateAcesCount($array)
    {
        return array_filter($array, function ($filterCards) {
            return $filterCards['seniority'] == 'ace';
        });
    }

    public function getSameCards($param, $cards, $card)
    {
        return array_filter($cards, function ($filterCards) use ($card, $param) {
            return $filterCards[$param] == $card[$param];
        });
    }
}
