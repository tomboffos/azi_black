<?php

namespace Database\Seeders;

use App\Contracts\WithdrawStatusContract;
use App\Models\WithdrawStatus;
use Illuminate\Database\Seeder;

class WithdrawStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        WithdrawStatus::insert(WithdrawStatusContract::STATUSES);
    }
}
