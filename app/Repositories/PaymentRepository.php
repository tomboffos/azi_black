<?php

namespace App\Repositories;

use App\Contracts\PaymentContract;
use App\Http\Requests\Api\Paymnet\PaymentStoreRequest;
use App\Models\Payment;
use App\Repositories\Interfaces\PaymentRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class PaymentRepository implements PaymentRepositoryInterface
{
    private $shopId = 2484;
    private $token = '36b04c0df24a5a79b5e1256a90d2b73b';
    private $baseUrl = 'https://lk.rukassa.is/api/v1';
    private $skyPayUrl = 'https://papi.skycrypto.me/rest/v2/purchases';
    private $skyPayToken = '4ff383b305f14d81b3d8858e41ee2eb5';

    public function savePayment(PaymentStoreRequest $request)
    {
        $payment = Payment::create(array_merge($request->validated(), [
            PaymentContract::USER_ID => $request->user()->id,
            PaymentContract::STATUS => 1
        ]));

        $paymentGateway = $request->input('paymentGateway', 'rukassa');

        if ($paymentGateway === 'skypay') {
            return $this->generateSkyPayLink($payment);
        }

        return $this->generateLink($payment);
    }

    public function generateLink(Payment $payment)
    {
        $data = [
            'shop_id' => $this->shopId,
            'token' => $this->token,
            'order_id' => (string)$payment->id,
            'amount' => $payment->amount,
            'user_code' => 7150 + rand(1, 9),
            'data' 		=> json_encode([
                'order_id' => (string)$payment->id,
            ]),
        ];

        $query = http_build_query($data);
        $url = "{$this->baseUrl}/create?" . $query;

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $result = json_decode(curl_exec($ch));
        curl_close($ch);

        return response(['link' => $result->url]);
    }

    public function generateSkyPayLink(Payment $payment)
    {
        $data = [
            'amount' => (int)$payment->amount,
            'label' => (string)$payment->id,
            'symbol' => 'usdt', // or other currency you want to use
            'currency' => 'kzt', // or other fiat currency
            'is_currency_amount' => true,
            'back_url' => 'https://shamai.online',
        ];

        $response = Http::withHeaders([
            'Authorization' => 'Token ' . $this->skyPayToken,
        ])->post($this->skyPayUrl, $data);

        $result = $response->json();

        return response(['link' => $result['web_link']]);
    }

    public function paymentStatusCallback(Request $request)
    {
        Log::info('request of payment ' . json_encode($request->all()));
        $payment = Payment::find($request->input('label'));
        if ($request->has('confirmed_at')) {
            $payment->update([
                PaymentContract::STATUS => 2
            ]);
        } else {
            $payment->update([
                PaymentContract::STATUS => 3
            ]);
        }

        return response("RESULT=OK", 200);
    }
}
