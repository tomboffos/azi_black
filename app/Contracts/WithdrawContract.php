<?php

namespace App\Contracts;

interface WithdrawContract
{
    const USER_ID = 'user_id';
    const AMOUNT = 'amount';
    const PERCENT = 'percent';
    const STATUS_ID = 'withdraw_status_id';
    const NAME = 'name';
    const CARD = 'card';


    const FILLABLE = [
        self::USER_ID,
        self::AMOUNT,
        self::PERCENT,
        self::STATUS_ID,
        self::CARD,
        self::NAME
    ];
}
