<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        $this->call([
//            SettingSeeder::class,
//            PaymentStatusSeeder::class,
//            StatusSeeder::class,
            RoomSeeder::class,
//            WithdrawStatusSeeder::class,
        ]);
    }
}
