<?php

namespace App\Observers;

use App\Http\Resources\Game\GameResource;
use App\Models\GameMember;
use App\Service\WebSocketService;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class GameMemberObserver
{
    /**
     * Handle the GameMember "created" event.
     *
     * @param  \App\Models\GameMember  $gameMember
     * @return void
     */
    public function created(GameMember $gameMember)
    {
        WebSocketService::sendEvent('azi.player.added.'.$gameMember->game->id, new GameResource($gameMember->game));
    }

    /**
     * Handle the GameMember "updated" event.
     *
     * @param  \App\Models\GameMember  $gameMember
     * @return void
     */
    public function updated(GameMember $gameMember)
    {
        if($gameMember->getOriginal('ready') != $gameMember->ready) {
            WebSocketService::sendEvent('azi.player.ready.' . $gameMember->game->id, new GameResource($gameMember->game));
            Log::error('hi hui'.GameMember::where('game_id', $gameMember->game->id)->where('ready', 1)->count().$gameMember->game->room);
            if (GameMember::where('game_id', $gameMember->game->id)->where('ready', 1)->count() == $gameMember->game->room->users) {
                $gameMember->game->update([
                    'started_at' => Carbon::now()
                ]);
            }
        }

    }

    /**
     * Handle the GameMember "deleted" event.
     *
     * @param  \App\Models\GameMember  $gameMember
     * @return void
     */
    public function deleted(GameMember $gameMember)
    {
        //

    }

    /**
     * Handle the GameMember "restored" event.
     *
     * @param  \App\Models\GameMember  $gameMember
     * @return void
     */
    public function restored(GameMember $gameMember)
    {
        //
    }

    /**
     * Handle the GameMember "force deleted" event.
     *
     * @param  \App\Models\GameMember  $gameMember
     * @return void
     */
    public function forceDeleted(GameMember $gameMember)
    {
        //
    }
}
