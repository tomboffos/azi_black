<?php


namespace App\Repositories;


use App\Contracts\FriendshipContract;
use App\Http\Requests\Friendship\InviteRequest;
use App\Http\Requests\Friendships\StoreFriendshipRequest;
use App\Http\Resources\Friendships\FriendshipResource;
use App\Http\Resources\Game\GameResource;
use App\Http\Resources\User\UserResource;
use App\Models\Friendship;
use App\Models\Game;
use App\Models\User;
use App\Repositories\Interfaces\FriendshipRepositoryInterface;
use App\Service\WebSocketService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class FriendshipRepository implements FriendshipRepositoryInterface
{
    public function friendsList(Request $request)
    {

        return FriendshipResource::collection($request->user()->friends($request));
    }

    public function addFriend(StoreFriendshipRequest $request) : FriendshipResource
    {
        return new FriendshipResource(Friendship::create(array_merge($request->validated(), [
            FriendshipContract::USER_ID => $request->user()->id
        ])));
    }

    public function deleteFriend(Friendship $friendship) : Response
    {
        $friendship->delete();

        return response(['friend' => $friendship], 200);
    }


    public function confirmRequest(Friendship $friendship) : FriendshipResource
    {
        $friendship->update([
            FriendshipContract::CONFIRMED => 1
        ]);

        return new FriendshipResource($friendship);
    }


    public function inviteFriend(InviteRequest $request)
    {
        WebSocketService::sendEvent('azi.game.invite.' . $request->user_id, new GameResource(Game::find($request->game_id)));
    }

    public function requests(Request $request)
    {
        return FriendshipResource::collection($request->user()->friendRequests($request));
    }

    public function offers(Request $request)
    {
        return FriendshipResource::collection($request->user()->offers($request));
    }

    public function search(Request $request)
    {
        return UserResource::collection(User::where('name','iLIKE','%'.$request->search)->whereDoesntHave('userFriends',function($query) use ($request){
            $query->where('friend_id',$request->user()->id);
        })->whereDoesntHave('friendUsers',function($query) use ($request){
            $query->where('user_id',$request->user()->id);
        })->get());
    }
}
