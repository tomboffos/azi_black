<?php


namespace App\Repositories\Interfaces;


use App\Http\Requests\Game\StoreGameRequest;
use App\Http\Requests\Game\UpdateGameRequest;
use App\Models\Game;
use App\Models\Room;
use App\Models\User;
use Illuminate\Http\Request;

interface GameRepositoryInterface
{
    public function createGame(StoreGameRequest $request);

    public function createGameFromData(int $roomId, int $bid) : Game;

    public function updateGame(UpdateGameRequest $request, Game $game);

    public function showGame(Game $game);

    public function deleteGame(Game $game);

    public function enterRoom(Request $request);

    public function leave(Room $room, Request $request);

    public function leaveBot(Room $room, User $user);

    public function readyToPlay(Request $request,Game $game);
}
