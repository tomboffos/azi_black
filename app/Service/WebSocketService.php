<?php


namespace App\Service;


use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class WebSocketService
{

    public static function sendEvent($event, $data)
    {
        $response = Http::post('https://socket-server.app-rent.kz', [
            'event' => $event,
            'data' => $data
        ]);

        Log::error('chat status ' . $event);
    }
}
