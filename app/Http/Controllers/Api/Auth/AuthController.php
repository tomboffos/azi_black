<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Auth\LoginRequest;
use App\Http\Requests\Api\Auth\RegisterRequest;
use App\Http\Resources\User\UserResource;
use App\Repositories\Interfaces\AuthRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    protected $authRepository;

    public function __construct(AuthRepositoryInterface $authRepository)
    {
        $this->authRepository = $authRepository;
    }

    public function firstRegister(Request $request)
    {
        return $this->authRepository->firstRegister($request);
    }

    public function register(RegisterRequest $request)
    {
        $user = $this->authRepository->registration($request);

        return response([
            'user' => new UserResource($user),
            'token' => $user->createToken(Str::random(10))->plainTextToken
        ], 201);
    }

    public function login(LoginRequest $request)
    {
        $user = $this->authRepository->login($request);
        if ($user) {
            return response([
                'user' => new UserResource($user),
                'token' => $user->createToken(Str::random(10))->plainTextToken
            ], 200);
        }else{
            return response([
                'message' => 'Не правильный email или пароль'
            ], 400);
        }
    }
}
