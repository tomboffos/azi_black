<?php

namespace App\Http\Requests\Game;

use Illuminate\Foundation\Http\FormRequest;

class StoreGameRequest extends FormRequest
{
    public $roomId;
    public $bid;

    public function __construct(int $roomId, int $bid)
    {
        $this->bid = $bid;
        $this->roomId = $roomId;

    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'bid' => 'required',
            'room_id' => 'required'
        ];
    }


}
