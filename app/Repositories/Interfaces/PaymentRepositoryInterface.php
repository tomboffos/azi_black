<?php

namespace App\Repositories\Interfaces;

use App\Http\Requests\Api\Paymnet\PaymentStoreRequest;
use App\Models\Payment;
use Illuminate\Http\Request;

interface PaymentRepositoryInterface
{
    public function savePayment(PaymentStoreRequest $request);

    public function generateLink(Payment $payment);

    public function paymentStatusCallback(Request $request);
}
