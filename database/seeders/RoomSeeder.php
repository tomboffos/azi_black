<?php

namespace Database\Seeders;

use App\Contracts\RoomContract;
use App\Models\Room;
use Illuminate\Database\Seeder;

class RoomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        for ($i = 1000; $i<=3000; $i+=1000){
            for ($j = 0; $j<11;$j++){
//                Room::create([
//                   RoomContract::BID => $i,
//                   RoomContract::USERS => 2,
//                   RoomContract::ROOM_TYPE_ID => 1
//                ]);

//                Room::create([
//                    RoomContract::BID => $i,
//                    RoomContract::USERS => 4,
//                    RoomContract::ROOM_TYPE_ID => 1
//                ]);

                Room::create([
                    RoomContract::BID => $i,
                    RoomContract::USERS => 2,
                    RoomContract::ROOM_TYPE_ID => 2
                ]);
//
//
//                Room::create([
//                    RoomContract::BID => $i,
//                    RoomContract::USERS => 4,
//                    RoomContract::ROOM_TYPE_ID => 2
//                ]);
//
//                Room::create([
//                    RoomContract::BID => $i,
//                    RoomContract::USERS => 3,
//                    RoomContract::ROOM_TYPE_ID => 1
//                ]);
//
//                Room::create([
//                    RoomContract::BID => $i,
//                    RoomContract::USERS => 3,
//                    RoomContract::ROOM_TYPE_ID => 2
//                ]);
            }
        }

        for ($i = 5000 ; $i<=100000; $i+=5000){
            for ($j=0;$j<6;$j++){
//                Room::create([
//                    RoomContract::BID => $i,
//                    RoomContract::USERS => 2,
//                    RoomContract::ROOM_TYPE_ID => 1
//                ]);

//                Room::create([
//                    RoomContract::BID => $i,
//                    RoomContract::USERS => 4,
//                    RoomContract::ROOM_TYPE_ID => 1
//                ]);

//                Room::create([
//                    RoomContract::BID => $i,
//                    RoomContract::USERS => 2,
//                    RoomContract::ROOM_TYPE_ID => 1
//                ]);
//

                Room::create([
                    RoomContract::BID => $i,
                    RoomContract::USERS => 2,
                    RoomContract::ROOM_TYPE_ID => 2
                ]);
//
//                Room::create([
//                    RoomContract::BID => $i,
//                    RoomContract::USERS => 3,
//                    RoomContract::ROOM_TYPE_ID => 1
//                ]);
//
//                Room::create([
//                    RoomContract::BID => $i,
//                    RoomContract::USERS => 3,
//                    RoomContract::ROOM_TYPE_ID => 2
//                ]);
            }
        }
    }
}
