<?php

use App\Contracts\GameMemberContract;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGameMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_members', function (Blueprint $table) {
            $table->id();
            $table->foreignId(GameMemberContract::USER_ID)->constrained();
            $table->foreignId(GameMemberContract::GAME_ID)->constrained();
            $table->boolean(GameMemberContract::READY)->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('game_members');
    }
}
