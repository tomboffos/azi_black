<?php

namespace App\Repositories\Interfaces;

use App\Models\Game;
use App\Models\Room;

interface BotRepositoryInterface
{
    public function attachBotToRoom(Game $game);
}
