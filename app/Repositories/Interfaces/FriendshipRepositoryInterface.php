<?php


namespace App\Repositories\Interfaces;


use App\Http\Requests\Friendship\InviteRequest;
use App\Http\Requests\Friendships\StoreFriendshipRequest;
use App\Models\Friendship;
use App\Models\User;
use Illuminate\Http\Request;

interface FriendshipRepositoryInterface
{
    public function friendsList(Request $request);

    public function addFriend(StoreFriendshipRequest $request);

    public function deleteFriend(Friendship $friendship);

    public function confirmRequest(Friendship $friendship);

    public function inviteFriend(InviteRequest $request);

    public function requests(Request $request);

    public function offers(Request $request);

    public function search(Request $request);
}
