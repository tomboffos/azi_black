<?php

namespace App\Models;

use App\Contracts\PaymentStatusContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentStatus extends Model
{
    use HasFactory,SoftDeletes;

    protected $fillable = PaymentStatusContract::FILLABLE;
}
