<?php


namespace App\Repositories\Interfaces;


use App\Http\Requests\Room\StoreRoomRequest;
use App\Http\Requests\Room\UpdateRoomRequest;
use App\Models\Room;
use Illuminate\Http\Request;

interface RoomRepositoryInterface
{
    public function listRoomsByTypes(Request $request);

    public function storeRoom(StoreRoomRequest $request);

    public function updateRoom(UpdateRoomRequest $request, Room $room);

    public function showRoom(Room $room);

    public function deleteRoom(Room $room);

    public function filter(Request $request);
}
