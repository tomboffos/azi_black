<?php

namespace App\Contracts;

interface PaymentContract
{
    const USER_ID = 'user_id';
    const AMOUNT = 'amount';
    const STATUS = 'payment_status_id';
    const PAYMENT_TYPE = 'payment_type';


    const TYPES = [
        'card',
        'webmoney',
        'webmoney_z',
        'qiwi',
        'w1',
        'yandex',
        'ekzt',
        'btc',
        'onay'
    ];

    const FILLABLE = [
        self::USER_ID,
        self::AMOUNT,
        self::STATUS,
        self::PAYMENT_TYPE
    ];
}
