<?php

namespace App\Http\Requests\Api\Payment;

use App\Contracts\WithdrawContract;
use Illuminate\Foundation\Http\FormRequest;

class WithdrawStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            WithdrawContract::AMOUNT => 'required|numeric|min:150',
            WithdrawContract::NAME => 'required',
            WithdrawContract::CARD => 'required|min:16'
        ];
    }
}
