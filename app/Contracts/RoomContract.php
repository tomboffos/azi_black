<?php


namespace App\Contracts;


interface RoomContract
{
    const ROOM_TYPE_ID = 'room_type_id';
    const USERS = 'users';
    const BID = 'bid';
    const FILLABLE = [
        self::ROOM_TYPE_ID,
        self::USERS,
        self::BID
    ];
}
