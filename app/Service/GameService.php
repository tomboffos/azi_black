<?php


namespace App\Service;


use App\Contracts\GameContract;
use App\Contracts\GameMemberContract;
use App\Contracts\UserContract;
use App\Http\Resources\Game\GameResource;
use App\Models\Game;
use App\Models\GameMember;
use App\Models\Room;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class GameService
{
    public static function addMember(Request $request, Game $game)
    {
        if (
            !GameMember::where(GameMemberContract::USER_ID, $request->user()->id)->where(GameMemberContract::GAME_ID, $game->id)->exists()
        )
            GameMember::create([
                GameMemberContract::USER_ID => $request->user()->id,
                GameMemberContract::GAME_ID => $game->id
            ]);
    }

    public static function addUser(User $user, Game $game): GameMember
    {
        return GameMember::create([
            GameMemberContract::USER_ID => $user->id,
            GameMemberContract::GAME_ID => $game->id,
            GameMemberContract::VERIFIED => 0
        ]);
    }

    private $cardsService;

    public function gameStarted(Game $game)
    {
        $this->cardsService = new CardsService();

        $game->updateQuietly([
            GameContract::CENTRAL_CARD => $this->cardsService->getRandomCardFromDeck(true),
            GameContract::BID => $game->gameMembers()->count() * $game->room->bid,
        ]);

        foreach ($game->gameMembersArray() as $gameMember) {
            $user = $gameMember->user;

            $cards = $this->cardsService->giveCardsForUser();

            $gameMember->update([
                GameMemberContract::CARDS => $cards,
                GameMemberContract::POINTS => $this->cardsService->calculateCardsPoint($cards),
            ]);


            $this->restrictOrAddBalance($game->room->room_type_id, $user, 0, $game->room->bid);
        }

        $members = GameMember::where(GameMemberContract::GAME_ID, $game->id);

        $maxPoints = $members->max('points');

        if ($members->where(GameMemberContract::VERIFIED, false)->exists()) {
            $this->cardsService->givePointToUser($members->where(GameMemberContract::VERIFIED, false)->first(), $maxPoints);
        }

        $maxPoints = GameMember::where(GameMemberContract::GAME_ID, $game->id)->max('points');

        $winners = GameMember::where(GameMemberContract::GAME_ID, $game->id)->where(GameMemberContract::POINTS, $maxPoints);


        foreach ($winners->get() as $winner) {
            Log::info('winners ' . $winners->count());
            Log::info('winners ' . $winners->get());
            $this->restrictOrAddBalance($game->room->room_type_id, $winner->user, 1, $game->bid / $winners->count());
        }

        $game->update([
            GameContract::FINISHED_AT => Carbon::now()
        ]);

        $this->addNewGame($game, $game->gameMembers);


    }

    public function addNewGame(Game $lastGame, $gameMembers)
    {
        $game = Game::create([
            GameContract::ROOM_ID => $lastGame->room_id,
            GameContract::BID => 0
        ]);

        foreach ($gameMembers as $member) {
            $member->delete();

            if (GameService::checkBalanceOfUser(User::find($member->user_id), $game->room))
                GameMember::create([
                    GameMemberContract::USER_ID => $member->user_id,
                    GameMemberContract::GAME_ID => $game->id
                ]);
            else
                WebSocketService::sendEvent('azi.user.leave.room.' . $member->user_id, ['message' => 'У вас недостаточно баланса']);
        }

        WebSocketService::sendEvent('azi.game.started.' . $game->room_id, new GameResource($game));

    }


    public function restrictOrAddBalance($roomTypeId, $user, $add, $quantity)
    {

        switch ($roomTypeId) {
            case 1:
                $user->update([
                    UserContract::FAKE_BALANCE => $add ? $user->fake_balance + $quantity : $user->fake_balance - $quantity
                ]);
                break;

            case 2:
                $user->update([
                    UserContract::BALANCE => $add ? $user->balance + $quantity : $user->balance - $quantity
                ]);
                break;

        }
    }

    public static function checkBalanceOfUser(User $user, Room $room)
    {

        if ($room->room_type_id == 1 && $user->fake_balance < $room->bid)
            return false;

        if ($room->room_type_id == 2 && $user->balance < $room->bid)
            return false;

        return true;
    }
}
