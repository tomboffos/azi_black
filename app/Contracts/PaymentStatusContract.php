<?php

namespace App\Contracts;

interface PaymentStatusContract
{
    const NAME = 'name';

    const FILLABLE = [
        self::NAME
    ];

    const STATUSES = [
        [
            'id' => 1,
            'name' => 'В ожидании'
        ],
        [
            'id' => 2,
            'name' => 'Успешно'
        ],
        [
            'id' => 3,
            'name' => 'Ошибка'
        ]
    ];
}
