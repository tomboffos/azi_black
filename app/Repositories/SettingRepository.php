<?php


namespace App\Repositories;


use App\Http\Resources\Settings\SettingResource;
use App\Models\Setting;
use App\Repositories\Interfaces\SettingRepositoryInterface;

class SettingRepository implements SettingRepositoryInterface
{
    public function listOfSettings()
    {
        return SettingResource::collection(Setting::get());
    }
}
