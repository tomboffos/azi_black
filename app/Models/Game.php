<?php

namespace App\Models;

use App\Contracts\GameContract;
use App\Contracts\GameMemberContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Game extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = GameContract::FILLABLE;

    public function room()
    {
        return $this->belongsTo(Room::class, 'room_id', 'id');
    }

    public function members()
    {
        return $this->belongsToMany(User::class, 'game_members');
    }

    public function ifPlayerExistsInGame(User $user)
    {
        return GameMember::where([
            'user_id' => $user->id,
            'game_id' => $this->id
        ]);
    }

    public function gameMembers()
    {
        return $this->hasMany(GameMember::class);
    }


    public function gameMembersArray()
    {
        return $this->hasMany(GameMember::class)->orderBy(GameMemberContract::VERIFIED, 'desc')->get();
    }


    public function getBot()
    {
        return $this->gameMembers()->where(GameMemberContract::VERIFIED, false)->first();
    }


    public function getActualMembers()
    {
        return $this->hasMany(GameMember::class)->whereNull('deleted_at')->get();
    }


}
