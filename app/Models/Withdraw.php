<?php

namespace App\Models;

use App\Contracts\WithdrawContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Withdraw extends Model
{
    use HasFactory,SoftDeletes;

    protected $fillable = WithdrawContract::FILLABLE;
}
