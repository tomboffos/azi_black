<?php

namespace Database\Seeders;

use App\Contracts\RoomTypeContract;
use App\Models\RoomType;
use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        RoomType::insert(RoomTypeContract::TYPES);
    }
}
