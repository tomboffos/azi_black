<?php


namespace App\Contracts;


interface DeviceContract
{
    const USER_ID = 'user_id';
    const TOKEN = 'token';

    const FILLABLE = [
        self::USER_ID,
        self::TOKEN
    ];
}
