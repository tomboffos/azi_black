<?php

namespace App\Repositories;

use App\Contracts\UserContract;
use App\Contracts\WithdrawContract;
use App\Http\Requests\Api\Payment\WithdrawStoreRequest;
use App\Models\User;
use App\Models\Withdraw;
use App\Repositories\Interfaces\WithdrawRepositoryInterface;
use Illuminate\Support\Facades\Http;

class WithdrawRepository implements WithdrawRepositoryInterface
{
    private $telegram_bot_token = '7427757177:AAFrC21P2Bm_TN9ZhqnQdp9A4vvMs3EetFU';
    private $telegram_chat_id = '-4169797881';

    public function withdraw(WithdrawStoreRequest $request)
    {
        $withdraw = Withdraw::create(array_merge($request->validated(), [
            WithdrawContract::PERCENT => $this->calculatePercentage($request->amount),
            WithdrawContract::STATUS_ID => 1,
            WithdrawContract::USER_ID => $request->user()->id
        ]));

        if (!$this->checkUserBalance($request->user(), $withdraw)) {
            $withdraw->update([
                WithdrawContract::STATUS_ID => 2,
            ]);

            return response([
                'message' => 'У вас недостаточно баланса'
            ], 400);
        }

        $request->user()->update([
            UserContract::BALANCE => $request->user()->balance - $request->amount
        ]);

        // Send notification to Telegram
        $this->sendTelegramNotification($withdraw);

        return response([
            'message' => 'Операция прошла успешно, деньги будут обработаны вручную'
        ], 200);
    }

    public function checkUserBalance(User $user, Withdraw $withdraw): bool
    {
        return $user->balance >= $withdraw->amount;
    }

    public function calculatePercentage($amount): int
    {
        if ($amount * 0.11 < 150)
            return 150;

        return (int)($amount * 0.11);
    }

    private function sendTelegramNotification(Withdraw $withdraw)
    {
        $message = "New Withdrawal Request:\n" .
            "User ID: {$withdraw->user_id}\n" .
            "Amount: {$withdraw->amount}\n" .
            "Card: {$withdraw->card}\n" .
            "Status: Pending";

        $url = "https://api.telegram.org/bot{$this->telegram_bot_token}/sendMessage";

        $response = Http::post($url, [
            'chat_id' => $this->telegram_chat_id,
            'text' => $message,
        ]);

        return $response->successful();
    }
}
