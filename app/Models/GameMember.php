<?php

namespace App\Models;

use App\Contracts\GameMemberContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GameMember extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = GameMemberContract::FILLABLE;

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function game()
    {
        return $this->belongsTo(Game::class, 'game_id', 'id');
    }
}
