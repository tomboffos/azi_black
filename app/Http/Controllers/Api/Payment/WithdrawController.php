<?php

namespace App\Http\Controllers\Api\Payment;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Payment\WithdrawStoreRequest;
use App\Repositories\Interfaces\WithdrawRepositoryInterface;
use App\Repositories\WithdrawRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class WithdrawController extends Controller
{
    private $withdrawRepository;
    public function __construct(WithdrawRepositoryInterface $withdrawRepository)
    {
        $this->withdrawRepository = $withdrawRepository;
    }


    public function index(Request $request)
    {
        //
    }


    public function store(WithdrawStoreRequest $request)
    {
        //
        return $this->withdrawRepository->withdraw($request);
    }


    public function show($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }

    public function callback(Request $request)
    {
        Log::error('info about callback' . json_encode($request->all()));
    }
}
