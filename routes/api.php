<?php

use App\Http\Controllers\Api\Auth\AuthController;
use App\Http\Controllers\Api\Bot\BotController;
use App\Http\Controllers\Api\Friendship\FriendshipController;
use App\Http\Controllers\Api\Game\GameController;
use App\Http\Controllers\Api\Payment\PaymentController;
use App\Http\Controllers\Api\Payment\WithdrawController;
use App\Http\Controllers\Api\Room\RoomController;
use App\Http\Controllers\Api\Settings\SettingController;
use App\Http\Controllers\Api\Transfer\TransferController;
use App\Http\Resources\User\UserResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {

    $user = $request->user();
    return response([
        'user' => new UserResource($user),
        'token' => $user->createToken(Str::random(10))->plainTextToken
    ], 200);
});

Route::get('/bids', [RoomController::class, 'filter']);

Route::post('/withdraw/callback', [WithdrawController::class, 'callback'])->name('withdraw_callback');
Route::post('/payment/callback', [PaymentController::class, 'callback'])->name('payment_callback');

Route::prefix('/auth')->group(function () {
    Route::post('/first-register', [AuthController::class, 'firstRegister']);
    Route::post('/registration', [AuthController::class, 'register']);
    Route::post('/login', [AuthController::class, 'login']);
});

Route::middleware('auth:sanctum')->group(function () {
    Route::resources([
        'game' => GameController::class,
        'transfer' => TransferController::class,
        'room' => RoomController::class,
        'withdraws' => WithdrawController::class,
        'payments' => PaymentController::class,
        'friends' => FriendshipController::class,
    ]);
    Route::post('/enter-room', [GameController::class, 'enterToRoom']);
    Route::post('/start/{game}', [GameController::class, 'start']);
    Route::post('/leave/{room}', [GameController::class, 'leave']);
    Route::post('/confirm/{friendship}', [FriendshipController::class, 'confirm']);

    Route::get('/offers', [FriendshipController::class, 'offers']);
    Route::get('/requests', [FriendshipController::class, 'requests']);

    Route::post('/confirm/{friendship}', [FriendshipController::class, 'confirm']);

    Route::get('/search/users', [FriendshipController::class, 'search']);


    Route::post('/bot/{game}', [BotController::class, 'join']);
});

Route::resources([
    'settings' => SettingController::class,
]);
