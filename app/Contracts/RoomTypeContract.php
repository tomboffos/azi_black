<?php


namespace App\Contracts;


interface RoomTypeContract
{
    const NAME = 'name';

    const FILLABLE = [
        self::NAME
    ];

    const TYPES = [
        ['id' => 1,
            'name' => 'Бесплатная игра'],
        ['id' => 2,
            'name' => 'Платная игра'],
    ];
}
