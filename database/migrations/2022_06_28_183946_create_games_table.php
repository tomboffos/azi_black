<?php

use App\Contracts\GameContract;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->id();
            $table->foreignId(GameContract::ROOM_ID)->constrained();
            $table->foreignId(GameContract::WINNER_ID)->nullable()->constrained('users');
            $table->unsignedBigInteger(GameContract::BID);
            $table->dateTime(GameContract::FINISHED_AT)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games');
    }
}
