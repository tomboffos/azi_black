<?php


namespace App\Contracts;


interface RoomMemberContract
{
    const USER_ID = 'user_id';
    const ROOM_ID = 'room_id';
    const BID = 'bid';
    const READY = 'ready';

    const FILLABLE = [
        self::USER_ID,
        self::ROOM_ID,
        self::BID,
        self::READY
    ];
}
