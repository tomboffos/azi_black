<?php


namespace App\Repositories;


use App\Contracts\UserContract;
use App\Http\Requests\Api\Auth\LoginRequest;
use App\Http\Requests\Api\Auth\RegisterRequest;
use App\Http\Resources\User\UserResource;
use App\Models\User;
use App\Repositories\Interfaces\AuthRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AuthRepository implements AuthRepositoryInterface
{
    public function firstRegister(Request $request)
    {
        $user = User::firstOrCreate([
            'device' => $request->device
        ]);


        if($user->name == null && $user->fake_balance == null && $user->balance == null ){
            $user->update([
                UserContract::NAME => 'Пользователь-' . $user->id,
                UserContract::FAKE_BALANCE => 10000,
                UserContract::BALANCE => 0,
            ]);
        }


        return response([
            'user' => new UserResource($user),
            'token' => $user->createToken(Str::random(10))->plainTextToken
        ], 201);
    }


    public function registration(RegisterRequest $request)
    {
        return User::updateOrCreate([
            'device' => $request->device,
        ], $request->except('device'));
    }

    public function login(LoginRequest $request)
    {
        $user = User::where('email', $request->email)->first();

        $lastUser = User::where('device', $request->device);

        if ($lastUser->exists())
            $lastUser->first()->update([
                UserContract::DEVICE => null
            ]);

        if (Hash::check($request->password, $user->password)) {
            $user->update([
                'device' => $request->device,
            ]);
            return $user;
        }
        return null;
    }
}
