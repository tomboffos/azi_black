<?php

namespace App\Http\Requests\Api\Paymnet;

use App\Contracts\PaymentContract;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Enum;

class PaymentStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            PaymentContract::AMOUNT => 'required',
            PaymentContract::PAYMENT_TYPE => 'required|in:'.implode(',',PaymentContract::TYPES),
            'paymentGateway' => 'sometimes|string|in:rukassa,skypay'
        ];
    }
}
