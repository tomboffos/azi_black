<?php


namespace App\Contracts;


interface GameMemberContract
{
    const USER_ID = 'user_id';
    const GAME_ID = 'game_id';
    const READY = 'ready';
    const CARDS = 'cards';
    const POINTS = 'points';
    const VERIFIED = 'verified';

    const FILLABLE = [
        self::USER_ID,
        self::GAME_ID,
        self::READY,
        self::CARDS,
        self::POINTS,
        self::VERIFIED
    ];
}
