<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use App\Contracts\FriendshipContract;
use App\Contracts\UserContract;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use CrudTrait;
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = UserContract::FILLABLE;

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = UserContract::HIDDEN;

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = UserContract::CASTS;

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public function friends(Request $request)
    {

        return Friendship::where(function ($query) {
            $query->where(FriendshipContract::USER_ID, $this->id)->orWhere(FriendshipContract::FRIEND_ID, $this->id);
        })->where(function ($query) use ($request) {
            if ($request->has('search')) {
                $query->where(function($second) use ($request){
                    $second->where(FriendshipContract::USER_ID, '!=', $this->id)->whereHas('user', function ($secondQuery) use ($request) {
                        $secondQuery->where('name', 'iLIKE', '%' . $request->search . '%');
                    });
                })->orWhere(function($second) use ($request){
                    $second->where(FriendshipContract::FRIEND_ID, '!=', $this->id)->whereHas('friend', function ($secondQuery) use ($request) {
                        $secondQuery->where('name', 'iLIKE', '%' . $request->search . '%');
                    });
                });
            }
        })->where(FriendshipContract::CONFIRMED, 1)->get();
    }

    public function friendRequests(Request $request)
    {
        return Friendship::where(FriendshipContract::USER_ID, $this->id)->where('confirmed', 0)->where(function ($query) use ($request) {
            if ($request->has('search'))
                $query->whereHas('friend', function ($query) use ($request) {
                    $query->where('name', 'iLIKE', '%' . $request->search . '%');
                });
        })->get();
    }

    public function offers(Request $request)
    {
        return Friendship::where(FriendshipContract::FRIEND_ID, $this->id)->where('confirmed', 0)->where(function ($query) use ($request) {
            if ($request->has('search'))
                $query->whereHas('user', function ($query) use ($request) {
                    $query->where('name', 'iLIKE', '%' . $request->search . '%');
                });
        })->get();
    }

    public function userFriends()
    {
        return $this->hasMany(Friendship::class);
    }

    public function friendUsers()
    {
        return $this->hasMany(Friendship::class,FriendshipContract::FRIEND_ID);
    }
}
