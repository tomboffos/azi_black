<?php

namespace App\Models;

use App\Contracts\GameContract;
use App\Contracts\RoomContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Room extends Model
{
    use HasFactory, SoftDeletes;

    public $fillable = RoomContract::FILLABLE;

    public function getCurrentGame()
    {
        return Game::where(GameContract::ROOM_ID,$this->id)->orderBy('id','desc');
    }

    public function currentGameMembers()
    {
        $gameQuery = Game::where(GameContract::ROOM_ID,$this->id)->orderBy('id','desc');
        return $gameQuery->exists() ? $gameQuery->first()->gameMembers()->count() : 0;
    }
}
