<?php

namespace App\Http\Resources\Friendships;

use App\Http\Resources\User\UserResource;
use Illuminate\Http\Resources\Json\JsonResource;

class FriendshipResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'friend' => new UserResource($request->user()->id == $this->user_id ? $this->friend : $this->user),
            'id' => $this->id
        ];
    }
}
