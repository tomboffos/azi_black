<?php


namespace App\Repositories\Interfaces;


interface SettingRepositoryInterface
{
    public function listOfSettings();
}
