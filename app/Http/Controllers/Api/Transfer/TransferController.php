<?php

namespace App\Http\Controllers\Api\Transfer;

use App\Http\Controllers\Controller;
use App\Http\Requests\Transfer\TransferStoreRequest;
use App\Http\Resources\Transfer\TransferResource;
use App\Http\Resources\User\UserResource;
use App\Models\User;
use App\Repositories\Interfaces\TransferRepositoryInterface;
use Illuminate\Http\Request;

class TransferController extends Controller
{
    protected $transferRepository;

    public function __construct(TransferRepositoryInterface $transferRepository)
    {
        $this->transferRepository = $transferRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  TransferStoreRequest  $request
     */
    public function store(TransferStoreRequest $request)
    {
        return $this->transferRepository->storeTransfer($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        return $this->transferRepository->showTransfer($request);
    }
}
