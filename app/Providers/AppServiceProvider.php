<?php

namespace App\Providers;

use App\Models\Game;
use App\Models\GameMember;
use App\Models\Payment;
use App\Observers\GameMemberObserver;
use App\Observers\GameObserver;
use App\Observers\PaymentObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RepositoryServiceProvider::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Game::observe(GameObserver::class);
        GameMember::observe(GameMemberObserver::class);
        Payment::observe(PaymentObserver::class);
    }
}
