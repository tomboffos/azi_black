<?php

namespace App\Http\Controllers\Api\Game;

use App\Http\Controllers\Controller;
use App\Http\Requests\Game\StoreGameRequest;
use App\Http\Requests\Game\UpdateGameRequest;
use App\Models\Game;
use App\Models\Room;
use App\Repositories\Interfaces\GameRepositoryInterface;
use Illuminate\Http\Request;

class GameController extends Controller
{
    protected $gameRepository;

    public function __construct(GameRepositoryInterface $gameRepository)
    {
        $this->gameRepository = $gameRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreGameRequest $request
     */
    public function store(StoreGameRequest $request)
    {
        return $this->gameRepository->createGame($request);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Game $game
     */
    public function show(Game $game)
    {
        return $this->gameRepository->showGame($game);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateGameRequest $request
     * @param \App\Models\Game $game
     */
    public function update(UpdateGameRequest $request, Game $game)
    {
        return $this->gameRepository->updateGame($request, $game);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Game $game
     */
    public function destroy(Game $game)
    {
        return $this->gameRepository->deleteGame($game);
    }


    public function enterToRoom(Request $request)
    {
        return $this->gameRepository->enterRoom($request);
    }

    public function leave(Room $room, Request $request)
    {
        return $this->gameRepository->leave($room, $request);
    }

    public function start(Game $game, Request $request)
    {
        return $this->gameRepository->readyToPlay($request, $game);
    }
}
