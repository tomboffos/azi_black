<?php


namespace App\Repositories;


use App\Contracts\UserContract;
use App\Http\Requests\Transfer\TransferStoreRequest;
use App\Http\Resources\Transfer\TransferResource;
use App\Http\Resources\Transfer\UserBalanceResource;
use App\Models\User;
use App\Repositories\Interfaces\TransferRepositoryInterface;
use Illuminate\Http\Request;

class TransferRepository implements TransferRepositoryInterface
{
    public function storeTransfer(TransferStoreRequest $request)
    {
        $user = $request->user();
        if ($user->balance >= $request->fake_balance / 100) {
            $user->update([
                UserContract::BALANCE => $user->balance - $request->fake_balance / 100,
                UserContract::FAKE_BALANCE => $user->fake_balance - $request->fake_balance
            ]);
            return response([
                'message' => 'transferred',
                'user' => new TransferResource($user)
            ], 200);
        } else {
            return response([
                'message' => 'Not enough balance'
            ], 400);
        }

    }

    public function showTransfer(Request $request)
    {
        return new UserBalanceResource($request->user());
    }
}
