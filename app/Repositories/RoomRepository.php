<?php


namespace App\Repositories;


use App\Http\Requests\Room\StoreRoomRequest;
use App\Http\Requests\Room\UpdateRoomRequest;
use App\Http\Resources\Room\RoomResource;
use App\Models\Room;
use App\Repositories\Interfaces\RoomRepositoryInterface;
use Illuminate\Http\Request;

class RoomRepository implements RoomRepositoryInterface
{
    public function listRoomsByTypes(Request $request)
    {
        return RoomResource::collection(Room::where('room_type_id', $request->room_type_id)
            ->where('bid', $request->bid)
            ->orderBy('users', 'asc')
            ->get());
    }

    public function storeRoom(StoreRoomRequest $request)
    {
        return new RoomResource(Room::create($request->validated()));
    }

    public function updateRoom(UpdateRoomRequest $request, Room $room)
    {
        $room->update($request->validated());
        return new RoomResource($room);
    }

    public function showRoom(Room $room)
    {
        return new RoomResource($room);
    }

    public function deleteRoom(Room $room)
    {
        if ($room->users < 2)
            return $room->delete();
        else
            return response([
                'message' => 'Clown'
            ]);
    }

    public function filter(Request $request)
    {
        return Room::where('room_type_id', $request->room_type_id)->distinct('bid')->pluck('bid');
    }
}
