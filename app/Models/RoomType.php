<?php

namespace App\Models;

use App\Contracts\RoomTypeContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RoomType extends Model
{
    use HasFactory, SoftDeletes;

    public $fillable = RoomTypeContract::FILLABLE;
}
