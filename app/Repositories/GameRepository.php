<?php


namespace App\Repositories;


use App\Contracts\GameContract;
use App\Contracts\GameMemberContract;
use App\Http\Requests\Game\StoreGameRequest;
use App\Http\Requests\Game\UpdateGameRequest;
use App\Http\Resources\Game\GameResource;
use App\Http\Resources\Room\RoomResource;
use App\Models\Game;
use App\Models\GameMember;
use App\Models\Room;
use App\Models\User;
use App\Repositories\Interfaces\GameRepositoryInterface;
use App\Service\GameService;
use App\Service\WebSocketService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class GameRepository implements GameRepositoryInterface
{
    public function createGame(StoreGameRequest $request)
    {
        return new GameResource(Game::create($request->validated()));
    }

    public function updateGame(UpdateGameRequest $request, Game $game)
    {
        $game->update($request->safe()->merge([
            'winner_id' => $request->winner_id
        ]));
        return new GameResource($game);
    }

    public function showGame(Game $game)
    {
        return new GameResource($game);
    }

    public function deleteGame(Game $game)
    {
        $game->delete();
        return response([
            'message' => 'Game deleted successfully'
        ]);
    }

    public function enterRoom(Request $request)
    {
        $room = Room::find($request->room_id);

        $currentGame = $room->getCurrentGame();
        Log::info('user : ' . json_encode($request->user()) . ' game: ' . json_encode($currentGame));
        if (!GameService::checkBalanceOfUser($request->user(), $room))
            return response(['message' => 'У вас недостаточно баланса'], 400);

        if ($currentGame->exists()) {
            $currentGameModel = $currentGame->first();


            if ($currentGameModel->gameMembers()->count() > $room->users)
                return response(['message' => "Количество пользователей достигло {$room->users}"], 400);

            GameService::addMember($request, $currentGameModel);

            WebSocketService::sendEvent('azi.room.changed', new RoomResource($currentGameModel->room));

            return new GameResource($currentGameModel);
        }

        $game = Game::create([
            GameContract::ROOM_ID => $request->room_id,
            GameContract::BID => 0,
        ]);

        GameService::addMember($request, $game);

        WebSocketService::sendEvent('azi.room.changed', new RoomResource($game->room));

        return new GameResource($game);
    }

    public function readyToPlay(Request $request, Game $game)
    {
        $gameMemberQuery = $game->ifPlayerExistsInGame($request->user());
        if (!$gameMemberQuery->exists())
            return response([], 404);

        $gameMemberQuery->first()->update([
            'ready' => 1
        ]);


        return new GameResource($game);
    }

    public function leave(Room $room, Request $request)
    {
        $game = $room->getCurrentGame()->first();
        if ($game != null) {
            GameMember::where('game_id', $game->id)->where('user_id', $request->user()->id)->delete();


            WebSocketService::sendEvent('azi.player.leave.' . $game->id, new GameResource($game));

            GameMember::where('game_id', $game->id)->where('verified', false)->delete();
        }
        WebSocketService::sendEvent('azi.room.changed', new RoomResource($room));
        return response([], 200);
    }

    public function leaveBot(Room $room, User $user)
    {
        $game = $room->getCurrentGame()->first();
        if ($game != null) {
            GameMember::where('game_id', $game->id)->where('verified', false)->delete();


            WebSocketService::sendEvent('azi.player.leave.' . $game->id, new GameResource($game));
        }
        WebSocketService::sendEvent('azi.room.changed', new RoomResource($room));
        $user->delete();
        return response([], 200);
    }

    public function createGameFromData(int $roomId, int $bid): Game
    {
        return Game::create([
            GameContract::BID => $bid,
            GameContract::ROOM_ID => $roomId
        ]);
    }
}
