<?php

namespace App\Console\Commands;

use App\Contracts\UserContract;
use App\Models\User;
use Illuminate\Console\Command;

class BalanceFill extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'balance:fill';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        foreach (User::where('fake_balance',0)->get() as $user){
            $user->update([
                UserContract::FAKE_BALANCE => 10000
            ]);
        }

        return 0;
    }
}
