<?php

namespace App\Contracts;

interface WithdrawStatusContract
{
    const NAME = 'name';

    const FILLABLE = [
        self::NAME
    ];

    const STATUSES = [
        [
            'id' => 1,
            'name' => 'В ожидании',
        ],
        [
            'id' => 2,
            'name' => 'Отклонено',
        ],
        [
            'id' => 3,
            'name' => 'Успешно'
        ]
    ];
}
