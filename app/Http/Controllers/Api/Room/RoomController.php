<?php

namespace App\Http\Controllers\Api\Room;

use App\Http\Controllers\Controller;
use App\Http\Requests\Room\StoreRoomRequest;
use App\Http\Requests\Room\UpdateRoomRequest;
use App\Models\Room;
use App\Repositories\Interfaces\RoomRepositoryInterface;
use Illuminate\Http\Request;

class RoomController extends Controller
{
    protected $roomRepository;

    public function __construct(RoomRepositoryInterface $roomRepository)
    {
        $this->roomRepository = $roomRepository;
    }

    /**
     * Display a listing of the resource.
     *
     */
    public function index(Request $request)
    {
        return $this->roomRepository->listRoomsByTypes($request);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreRoomRequest $request
     */
    public function store(StoreRoomRequest $request)
    {
        return $this->roomRepository->storeRoom($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Room  $room
     */
    public function show(Room $room)
    {
        return $this->roomRepository->showRoom($room);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateRoomRequest  $request
     * @param  \App\Models\Room  $room
     */
    public function update(UpdateRoomRequest $request, Room $room)
    {
        return $this->roomRepository->updateRoom($request, $room);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Room  $room
     */
    public function destroy(Room $room)
    {
        return $this->roomRepository->deleteRoom($room);
    }

    public function filter(Request $request)
    {
        return $this->roomRepository->filter($request);
    }
}
