<?php

use App\Contracts\PaymentContract;
use App\Contracts\PaymentStatusContract;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();
            $table->enum(PaymentContract::PAYMENT_TYPE,PaymentContract::TYPES);
            $table->foreignId(PaymentContract::USER_ID)->constrained();
            $table->unsignedBigInteger(PaymentContract::AMOUNT);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
