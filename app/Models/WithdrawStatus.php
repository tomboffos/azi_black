<?php

namespace App\Models;

use App\Contracts\WithdrawStatusContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WithdrawStatus extends Model
{
    use HasFactory,SoftDeletes;

    protected $fillable = WithdrawStatusContract::FILLABLE;
}
