<?php

namespace App\Models;

use App\Contracts\DeviceContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Device extends Model
{
    use HasFactory, SoftDeletes;

    public $fillable = DeviceContract::FILLABLE;

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
