<?php


namespace App\Contracts;


interface GameContract
{
    const ROOM_ID = 'room_id';
    const BID = 'bid';
    const WINNER_ID = 'winner_id';
    const FINISHED_AT = 'finished_at';
    const STARTED_AT = 'started_at';
    const CENTRAL_CARD = 'central_card';

    const FILLABLE = [
        self::ROOM_ID,
        self::BID,
        self::WINNER_ID,
        self::FINISHED_AT,
        self::STARTED_AT,
        self::CENTRAL_CARD
    ];

    const CASTS = [
        self::FINISHED_AT => 'datetime',
        self::STARTED_AT => 'datetime',
    ];
}
