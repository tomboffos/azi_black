<?php

use App\Contracts\UserContract;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddChangesToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string(UserContract::NAME)->nullable()->change();
            $table->string(UserContract::EMAIL)->nullable()->change();
            $table->string(UserContract::PASSWORD)->nullable()->change();
            $table->unsignedBigInteger(UserContract::FAKE_BALANCE)->default(10000);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
