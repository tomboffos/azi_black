<?php

namespace App\Http\Controllers\Api\Payment;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Paymnet\PaymentStoreRequest;
use App\Models\Payment;
use App\Repositories\Interfaces\PaymentRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PaymentController extends Controller
{
    private $paymentRepository;

    public function __construct(PaymentRepositoryInterface  $paymentRepository)
    {
        $this->paymentRepository = $paymentRepository;
    }

    public function index()
    {
        //
    }


    public function store(PaymentStoreRequest $request)
    {
        return $this->paymentRepository->savePayment($request);
    }


    public function show($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }

    public function callback(Request $request)
    {
        return $this->paymentRepository->paymentStatusCallback($request);
    }

    public function success()
    {

    }

    public function fail()
    {

    }
}
