<?php


namespace App\Repositories\Interfaces;


use App\Http\Requests\Transfer\TransferStoreRequest;
use App\Models\User;
use Illuminate\Http\Request;

interface TransferRepositoryInterface
{
    public function storeTransfer(TransferStoreRequest $request);

    public function showTransfer(Request $request);


}
