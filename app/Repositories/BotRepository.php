<?php

namespace App\Repositories;

use App\Contracts\GameMemberContract;
use App\Contracts\UserContract;
use App\Http\Resources\Room\RoomResource;
use App\Models\Game;
use Faker\Factory as Faker;

use App\Models\Room;
use App\Models\User;
use App\Repositories\Interfaces\BotRepositoryInterface;
use App\Repositories\Interfaces\GameRepositoryInterface;
use App\Service\GameService;
use App\Service\WebSocketService;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Str;

class BotRepository implements BotRepositoryInterface
{

    public function attachBotToRoom(Game $game)
    {
        for ($i = 0; $i < $game->room->users - 1; $i++) {
            if ($game->getActualMembers()->count() < $game->room->users){
                $faker = Faker::create();


                $user = User::create([
                    UserContract::BALANCE => $game->bid * 10,
                    UserContract::NAME => $faker->name,
                    UserContract::DEVICE => Str::random(10)
                ]);


                $gameMember = GameService::addUser($user, $game);

                WebSocketService::sendEvent('azi.room.changed', new RoomResource($game->room));
                sleep(1);
                $gameMember->update([
                    GameMemberContract::READY => 1,
                ]);

            }
        }

    }


}
